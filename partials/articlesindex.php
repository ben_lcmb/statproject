<?php


?>


<div class="container-lg articles pt-5">
    <div class="row">
        <div class="col-12 text-center">
            <h2>Quoi de neuf ici !!</h2>
        </div>
    </div>
    <div class="container-lg article">
        <div class="col-4">
            <div class="card first-article" style="width: 32rem;">
                <img src="assets/images/imagesarti.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Titre</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="../articles.php" class="btn btn-primary">voir articles</a>
                </div>
            </div>
        </div>
        <div class="col-7">
            <div class="col-4 secondary ">
                <div class="card second mb-5" style="width: 30rem;">
                    <div class="card-body">
                        <h5 class="card-title">titre</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="../articles.php" class="btn btn-primary">voir articles</a>
                    </div>
                </div>

                <div class="card third mt-5" style="width: 30rem;">
                    <div class="card-body">
                        <h5 class="card-title">titre</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="../articles.php" class="btn btn-primary">voir articles</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="row blogDirection">
        <div class="col-12 text-center">
            <h2><a href="../articles.php">Tout nos articles</a></h2>
        </div>
    </div>
</div>
