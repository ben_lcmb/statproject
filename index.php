<?php
require_once 'core/functions.php';

getHeader('accueil');

?>
    <div class="clippath"></div>
    <div class="clippathead" style="background-color: #d63384"></div>
    <div class="clippatharticle"></div>
    <div class="container">
    <?php getCarrousel (); ?>

    <?php getPresentation(); ?>
    <?php
    getArticlesIndex();
    ?>

</div>




<?php
getFooter();
