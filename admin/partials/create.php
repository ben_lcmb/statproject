<?php

getDatabaseConnexion();
use Cocur\Slugify\Slugify;




?>


<div class="col-5 newarticle">
    <h2>créer un article</h2>
    <form method="post" class="mt-4" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="title" class="form-label"></label>
            <input
                type="text"
                name="title"
                class="form-control"
                id="title"
                placeholder="Titre de mon article"
                required
                <?php if (isset($blog) && !empty($blog->title)): ?>
                    value="<?php echo $blog->title; ?>"
                <?php endif; ?>
            >
        </div>

        <div class="mb-3">
            <label for="image-file" class="form-label"></label>
            <input class="form-control" type="file" id="image-file" name="image-file">
        </div>

        <div class="mb-3">
            <label for="excerpt" class="form-label"></label>
            <input
                type="text"
                name="excerpt"
                class="form-control"
                id="excerpt"
                placeholder="Entête"
                <?php if (isset($blog) && !empty($blog->excerpt)): ?>
                    value="<?php echo $blog->excerpt; ?>"
                <?php endif; ?>
            >
        </div>

        <div class="mb-3">
            <label for="excerpt" class="form-label">Contenu de l'article</label>
            <textarea type="text" class="form-control" id="excerpt" name="content"
                      required><?php if (isset($blog) && !empty($blog->excerpt)): ?><?php echo $blog->excerpt; ?><?php endif; ?></textarea>
        </div>

        <div class="mb-3 text-end">
            <button type="submit" class="btn btn-primary">
                Publier
            </button>
        </div>

    </form>
</div>