<?php
require_once '../../core/functions.php';

use Cocur\Slugify\Slugify;

checkLogin();



if (!empty($_GET['blog'])) {

$db = getDatabaseConnexion();
$response = $db->query("SELECT * FROM `article` WHERE id = " . $_GET['article']);

$blog =  $response->fetchObject();
$errorMessage = null;

if(!empty($_POST)) {
$slugify = new Slugify(); //Permet de générer les slug

$data = $_POST;
$data['slug'] = $slugify->slugify($data['title']);
$data['file_name'] = uploadFile('image-file');

$request = $db->prepare("UPDATE `article` SET `title` = :title, `excerpt` = :excerpt, `slug` = :slug, `content` = :content, `image` = :file_name WHERE `article`.`id` = $blog->id;");

if($request->execute($data)) {
header('Location: /admin/adminboard.php');
} else {
$errorMessage = "Le portfolio ne peut pas être modifié";
}
}
} else {
header('Location: /admin/adminboard');
}
?>
<div class="container">
    <div class="row">
        <div class="col-12 text-end mt-3">
            <a href="/admin/adminboard.php">
                Retour à la liste
            </a>
        </div>
        <?php if (null !== $errorMessage): ?>
            <div class="col-12 mt-5">
                <div class="alert alert-danger" role="alert">
                    <?php echo $errorMessage; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="col-12">
            <?php include 'create.php'; ?>
        </div>
    </div>
</div>
