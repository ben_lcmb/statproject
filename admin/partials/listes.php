<?php
require_once '../core/functions.php';
$lastBlogs = getLastBlogs(4);
$perPage = 20;
$totalBlog = getTotalBlog();
$currentPage = 1;
if (!empty($_GET['page'])) {
    $currentPage = $_GET['page'];
}
$pages = ceil($totalBlog / $perPage);
$blogs = getPaginatedBLogs($perPage, $currentPage);
$currentPage = 1;
if (!empty($_GET['page'])) {
    $currentPage = $_GET['page'];
}
?>

<div class="col-6 listes">
    <div class="container mt-5">
        <div class="row">
            <div class="text-center">
                <?php foreach ($blogs as $blog): ?>
                    <div class="d-flex justify-content-between">
                            <span>
                                <?php echo $blog->title; ?>
                            </span>

                        <div>
                            <a href="/admin/partials/edit.php?portfolio=<?php echo $blog->id; ?>">
                                Modifier
                            </a>

                            <a href="/admin/partials/delete.php?portfolio=<?php echo $blog->id; ?>"
                               style="margin-left: 0.5rem"
                               onclick="return confirm('Etes-vous sûr de vouloir supprimer ?');">
                                Supprimer
                            </a>

                        </div>

                    </div>
                <?php endforeach; ?>
            </div>

            <?php if($pages > 1):?>
                <div class="col-12 justify-content-center mt-4">
                    <?php include '../../partials/pagination.php'; ?>
                </div>
            <?php endif;?>

        </div>
    </div>
</div>
