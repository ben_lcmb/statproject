<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] .'./vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'./config.php';

function getHeader(string $title)
{
    include $_SERVER['DOCUMENT_ROOT'] . './partials/header.php';
}
function getListes()
{
    include $_SERVER['DOCUMENT_ROOT'] . './admin/partials/listes.php';
}
function getCreate()
{
    include $_SERVER['DOCUMENT_ROOT'] . './admin/partials/create.php';
}

function getFooter()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/footer.php';
}

function getCarrousel()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/carrousel/carrousel.php';
}

function getPresentation()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/presentation.php';
}
function getArticlesIndex()
{
    include $_SERVER['DOCUMENT_ROOT'] .'./partials/articlesindex.php';
}
/**
 * @return PDO|null
 */
function getDatabaseConnexion(): ?PDO
{
    try {
        $host = 'mysql:host=localhost;dbname='. DB_NAME. ';charset=utf8';
        $db = new PDO($host, DB_USER, DB_PASSWD);
        return $db;
    } catch (Exception $e) {
        return null;
    }
}
/**
 * @param int $limit
 * @return array
 */
function getLastblogs(int $limit = 4): array
{
    if(null === $bd = getDatabaseConnexion()) {
        return [];
    }
    $response = $bd->query("SELECT * FROM `article` ORDER BY `createdAt` DESC LIMIT $limit");
    return $response->fetchAll(PDO::FETCH_OBJ);
}

/**
 * @param string $slug
 * @return mixed|null
 */
function getBlogBySlug(string $slug)
{
    if(null === $bd = getDatabaseConnexion()) {
        return null;
    }
    $response = $bd->query("SELECT * FROM `article` WHERE slug = '$slug' LIMIT 1");

    return $response->fetchObject();
}
function checkLogin(): void
{
    $currentUrl = $_SERVER['REQUEST_URI'];
    if(!isset($_SESSION['logged_in'])) {
        header('Location: /login1138.php?from='.urlencode($currentUrl));
    }
}
function getAdminBoard(string $title)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/adminboard.php';
}

function getAdminHeader(string $title)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/partials/header.php';
}


function getAdminFooter()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/partials/footer.php';
}

function getPaginatedBLogs($perPage = 10, $page = 1)
{
    if(null === $db = getDatabaseConnexion()) {
        return [];
    }

    $limit = $perPage;
    $offset = ($page - 1) * $perPage;
    $response = $db->query("SELECT * FROM `article` ORDER BY `createdAt` DESC LIMIT $offset,$limit");
    return $response->fetchAll(PDO::FETCH_OBJ);
}

function getTotalBlog(): int
{
    if(null === $db = getDatabaseConnexion()) {
        return 0;
    }

    $response = $db->query('SELECT COUNT(*) AS total FROM article');
    $result =  $response->fetch();
    return $result['total'];
}

/**
 * @param string $inputName
 * @return string|null
 */
function uploadFile(string $inputName): ?string
{
    if (isset($_FILES[$inputName]) && $_FILES[$inputName]["error"] == 0) {
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES[$inputName]["name"];
        $filetype = $_FILES[$inputName]["type"];
        $filesize = $_FILES[$inputName]["size"];

        //Verif format fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!array_key_exists($ext, $allowed)) {
            die("Erreur : Veuillez sélectionner un format de fichier valide.");
        }

        // Vérifie la taille du fichier - 5Mo maximum
        $maxsize = 5 * 1024 * 1024;
        if ($filesize > $maxsize) {
            die("Error: La taille du fichier est supérieure à la limite autorisée.");
        }

        if (in_array($filetype, $allowed)) {
            // Vérifie si le fichier existe avant de le télécharger.
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $filename)) {
                $filename = rand(0,99999) . '-' . $filename;
            }

            move_uploaded_file($_FILES[$inputName]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] ."/uploads/" . $filename);
            return $filename;
        } else {
            die("Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.");
        }
    } else {
        die("Error: " . $_FILES[$inputName]["error"]);
    }
}