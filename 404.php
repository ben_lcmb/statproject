<?php
require_once 'core/functions.php';

getHeader('404 not found');

?>

<div class="container ">
    <div class="row align-items-center justify-content-center mt-5 col-12">

        <img style="width: 75%; height: auto; " src="assets/images/404.png" alt="">
    </div>

</div>
