<?php
//session_start();
require_once 'core/functions.php';
//if ($_SESSION['logged_in'] === true) {
//    header('Location: /admin/adminboard.php');
//}
if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $db = getDatabaseConnexion();
    $req = $db->prepare('SELECT * FROM `admin` WHERE email = :email LIMIT 1;');
    $req->execute(['email' => $_POST['login'],
    ]);
    $admin = $req->fetchObject();
//    echo '<pre>';
//    var_dump($admin);
    if (false !== $admin && password_verify($_POST['password'], $admin->passwd)) {
        $_SESSION['logged_in'] = true;
        if (!empty($_GET['from'])) {
            header('Location: ' . $_GET['from']);
        } else {
            header('Location: /admin/adminboard.php');
        }
        exit();
    }
    header('Location: /login1138.php?status=error_login');
}

//getAdminBoard();

?>

<html lang="fr">

<link rel="stylesheet" href="build/style.css">
<body class="login-page">
<div class="container">
    <div class="raw align-content-center">
        <div class="col-6">
            <h2>Connexion</h2>
            <form method="post">
                <div class="mt-5">
                    <label for="login" class="form-label">Email</label> <br>
                    <input
                            type="email"
                            name="login"
                            class="form-control"
                            id="login"
                            aria-describedby="emailHelp"
                            required
                    >
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Mot de passe</label> <br>
                    <input type="password" name="password" class="form-control" id="password" required>
                </div>
                <button type="submit" class="btn btn-primary mb-5">
                    Connexion
                </button>
            </form>

        </div>
    </div>
</div>






