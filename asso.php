<?php
require_once 'core/functions.php';

getHeader("l'association");

?>

    <h2 class="text-center ">Notre Association</h2>

    <div class="asso container d-flex col-12 text-center mt-5 mb-5">
        <div class="col-5">
            <img src="assets/images/les-mordus-d-escalade-ont-retrouve-les-plaisirs-de-la_5315856.jpg" alt="">
        </div>
        <div class="bio col-6 align-content-center">
            <h2> Alors on Grimpe !!</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Ad aliquid aperiam architecto aspernatur assumenda at
            commodi deleniti dicta dolorem doloremque dolorum earum
            enim in magni maiores molestiae mollitia nihil non nulla
            officia porro possimus provident quisquam ratione,
            reiciendis reprehenderit repudiandae sapiente sed soluta
            vel veritatis voluptas voluptates voluptatibus. Amet assumenda
            at consequatur ea eveniet, ex ipsam minima natus nulla quam
            soluta suscipit tempora tenetur totam velit veritatis voluptate
            voluptatem? Animi architecto eaque ipsam non quas.</p>
        </div>
    </div>
    <div class=" asso container d-flex col-12 text-center mt-5 mb-5">

        <div class="bio col-6 align-content-center m-lg-5">

            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ad aliquid aperiam architecto aspernatur assumenda at
                commodi deleniti dicta dolorem doloremque dolorum earum
                enim in magni maiores molestiae mollitia nihil non nulla
                officia porro possimus provident quisquam ratione,
                reiciendis reprehenderit repudiandae sapiente sed soluta
                vel veritatis voluptas voluptates voluptatibus. Amet assumenda
                at consequatur ea eveniet, ex ipsam minima natus nulla quam
                soluta suscipit tempora tenetur totam velit veritatis voluptate
                voluptatem? Animi architecto eaque ipsam non quas.</p>

            <a  href="contact.php">Contactez nous</a>
        </div>
        <div class="col-5">
            <img src="assets/images/les-mordus-d-escalade-ont-retrouve-les-plaisirs-de-la_5315856.jpg" alt="">
        </div>
    </div>







<?php
getFooter();
